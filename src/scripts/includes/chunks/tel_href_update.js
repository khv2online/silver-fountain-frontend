( () => {

	let el = document.querySelector('.b-header__phone p'),
		 elVal,
		 elTel = $('.b-header__phone-link--mobile');

	let interval = setInterval( () => {
		elVal = $(el).text().replace(/[^\d]/g, '');;
		elTel.prop('href', `tel:+${elVal}`);
	},900);

	setTimeout( () => {
		clearInterval(interval);
	},3000);



})();