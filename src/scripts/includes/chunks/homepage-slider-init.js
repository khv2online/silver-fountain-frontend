let optsForSlider = {
    speed: 1000,
    spaceBetween: 100,
    onlyExternal: true,
    nextButton: $newView.find(".b-promo__arrow--right:first"),
    prevButton: $newView.find(".b-promo__arrow--left:first"),
    pagination: '.b-promo__pager',
    bulletClass: "b-promo__bullet",
    bulletActiveClass: "b-promo__bullet--active",
    buttonDisabledClass: "b-promo__arrow--disable",
    paginationClickable: true,
    effect: "fade",
    onSlideChangeStart: (swiper) => {

        let $currentSlide = swiper.slides.eq(swiper.previousIndex);
        let $nextSlide = swiper.slides.eq(swiper.realIndex);
        let $pager = $newView.find('.b-promo__pager:first');

        let $blot = $newView.find('.b-promo__miniblot:first');
        let $tail = $newView.find('.b-promo__miniblot-tail:first');

        let tl = new TimelineMax();
        tl
            .set($pager, { className: "+=b-promo__pager--disabled" })
            .addLabel("animeStart")
            .to($blot, 0.4, { x: `${(swiper.realIndex) * 26 }px` }, "animeStart")
            .to($blot, 0.2, { scaleY: 0.7, scaleX: 1.3 }, "animeStart")
            .to($tail, 0.09, { x: `${swiper.previousIndex < swiper.realIndex ? "-" : ""}4px` }, "animeStart")
            .addLabel("bloatHalfWay", "-=0.2")
            .to($blot, 0.2, { scaleY: 1, scaleX: 1 }, "bloatHalfWay")
            .to($tail, 0.04, { x: "0px", delay: -0.1 })
            .staggerTo($currentSlide.find('.b-promo__animated-item'), 0.4, {
                y: "-40px",
                opacity: 0.8,
            }, 0.1, "animeStart")
            .staggerFrom($nextSlide.find('.b-promo__animated-item'), 0.4, {
                y: "40px",
                opacity: 0.8,
            }, 0.1, "animeStart")
            .set($pager, { className: "-=b-promo__pager--disabled" });
    },
    onSlideChangeEnd: (swiper) => {
        let $prevSlide = swiper.slides.eq(swiper.previousIndex);
        TweenLite.set($prevSlide.find('.b-promo__animated-item'), { clearProps: "all" });
    },
    autoplay: 6000,
    fade: {
        crossFade: true
    },
    breakpoints: {
        // when window width is <= 1023px
        1023: {
            autoHeight: true,
            effect: false,
            onlyExternal: false,
        },
    }
};

let $elOfSlider = $newView.find('.b-promo__slider:first');
let homepagePromoSlider = new Swiper($elOfSlider, optsForSlider);

let titlesMiniSlider = new Swiper($newView.find(".b-title-switcher:first"), {
    direction: "vertical",
    speed: 980,
    onlyExternal: true,
    spaceBetween: 0,
    effect: "fade",
    fade: {
        crossFade: true,
    },
    slideActiveClass: "b-title-switcher__item--active",
    onSlideChangeStart: (swiper) => {

        let $currentTitle = swiper.slides.eq(swiper.previousIndex);
        let $nextTitle = swiper.slides.eq(swiper.realIndex);

        let tl = new TimelineMax();
        tl
            .addLabel("animeStart")
            .to($currentTitle.find('.b-title-switcher__text'), 0.4, {
                y: "-30px",
                opacity: 0.5,
                scale: 0.99,
            }, "animeStart")
            .from($nextTitle.find('.b-title-switcher__text'), 0.4, {
                y: "30px",
                opacity: 0.5,
                scale: 0.99,
            }, "animeStart");
    },
    onSlideChangeEnd: (swiper) => {

        let $slides = swiper.slides;
        TweenLite.set($slides.find('.b-title-switcher__text'), { clearProps: "all" });
    },
});
// TODO: fix click handler on homepage slider last slide link
$(_.last(homepagePromoSlider.slides)).css('pointer-events', 'none');
/*$(_.last(homepagePromoSlider.slides).getElementsByTagName('a')).each(function(index, el) {
    $(el).on('click', function(event) {
        event.preventDefault();
        let filtersParams = event.currentTarget.dataset.filtersParams.split(",");

        let seriazedFilsterStates = JSON.stringify({
            "rooms[]": [parseInt(filtersParams[0])],
            "plantype[]": [parseInt(filtersParams[1])]
        });

        localStorage.setItem("savedFlatFilterStates", seriazedFilsterStates);

        $(`<a href="/flats/filter/" style="display:none!important"></a>`).appendTo($newView).trigger('click');
    });
});*/

homepagePromoSlider.params.control = titlesMiniSlider;

$(window).on('resize.homepageEvents orientationchange.homepageEvents', _.debounce(() => {
    let currentSlideNum = homepagePromoSlider.realIndex;
    homepagePromoSlider.destroy(true, true);

    let breakpoint = getWindowSizeBreakpoint();

    if (breakpoint === "md" || breakpoint === "lg") {
        optsForSlider.autoHeight = false;
        optsForSlider.effect = "fade";
        optsForSlider.onlyExternal = true;
        optsForSlider.fade = {
            crossFade: true
        };
    } else {
        optsForSlider.autoHeight = true;
        optsForSlider.effect = false;
        optsForSlider.fade = false;
        optsForSlider.onlyExternal = false;
    }
    homepagePromoSlider = new Swiper($elOfSlider, optsForSlider);
    homepagePromoSlider.slideTo(currentSlideNum, 0);

}, 200));