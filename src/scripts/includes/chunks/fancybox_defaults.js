$.extend($.fancybox.defaults, {
    hash: false,
    infobar: true,
    buttons: [
        'close'
    ],
    margin: [40, 40],
    lang: 'ru',
    transitionEffect: "slide",
    idleTime: 0,
    clickContent: false,
    zoomOpacity: true,
    touch: {
        vertical: false, // Allow to drag content vertically
        momentum: false // Continuous movement when panning
    },
    i18n: {
        'ru': {
            CLOSE: 'Закрыть',
            NEXT: 'Далее',
            PREV: 'Назад',
            ERROR: 'Не удалось открыть галерею - попробуйте позже',
            PLAY_START: 'Запустить слайдшоу',
            PLAY_STOP: 'Пауза',
            FULL_SCREEN: 'На весь экран',
            THUMBS: 'Миниатуры'
        },
    },
    afterClose: function() {
        if (this.type === "inline") {

            let $form = this.$content.find('form:first');
            let validator = $form.trigger('reset').data("validator");
            if (validator) {
                validator.resetForm();
                $form.find('input.error').removeClass('error');
            }

            let $successText = this.$content.find('.b-form-success:first');
            if ($successText.length) {
                $form.parent().show();
                $successText.remove();
            }
        }
    }
});