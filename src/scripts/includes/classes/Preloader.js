class Preloader {
    constructor(id) {
        this.isLocked = false;
        this.el = document.querySelector(id);
        this.el.onclick = (event) => {
            event.preventDefault();
            event.stopPropagation();
            return false;
        };
    }

    lock() {
        this.isLocked = true;
    }

    unlock() {
        this.isLocked = false;
    }

    show(cb) {
        if (this.isLocked) return false;
        this.el.classList.add("b-preloader--active");
        if (cb) cb();
    }

    hide(cb) {
        if (this.isLocked) return false;
        this.el.classList.remove("b-preloader--active");
        if (cb) cb();
    }

}