class MainMenu {
    constructor(opts) {
        this.isOpen = false;

        let $modal = $(opts.modalWindowSelector);
        let $menu = $modal.find('.b-menu:first');
        let $bg = $menu.find('.b-menu__bg:first');
        let $bgUnderlay = $menu.find('.b-menu__bg-underlay:first');
        let $list = $menu.find('.b-menu__inner:first');
        let $toggler = $(opts.togglerSelector);

        this.maxBgNum = 0;

        preloadPictures([
            "/img/menu/1.jpg",
            "/img/menu/2.jpg",
            "/img/menu/3.jpg",
            "/img/menu/4.jpg"
        ], () => {
            this.maxBgNum += 1;
        });

        $modal.on('shown.bs.modal', () => {
            let tl = new TimelineMax();
            tl
                .set($list, { opacity: "0", x: "-20%" })
                .set($bg, { width: "0%" })
                .addLabel("animeStart")
                .to($bg, 0.3, { width: "100%" }, "animeStart")
                .to($bgUnderlay, 0.3, {
                    width: "70%",
                    delay: 0.08,
                    //ease:  Power0.easeNone,
                }, "animeStart")

                .to($list, 0.3, { x: "0%", opacity: "1", delay: 0.2 }, "animeStart")
                .set([$list, $bg], { clearProps: "all" })
                .set($bg, {className: "+=b-menu__bg--zooming"});
        });

        $toggler.on('click', (event) => {
            event.preventDefault();
            this.toggle();
        });

        $list.find('.b-menu__leaf').each((index, el) => {
            let $el = $(el);
            let $submenu = $el.find('.b-menu__submenu');


            if ($submenu.length) {
                $el.find("a:first").on('click', (event) => {
                    event.preventDefault();
                    this.openSubList($el, $submenu);

                });
            }
        });

        this.$modal = $modal;
        this.$bg = $bg;
        this.$list = $list;
        this.$toggler = $toggler;
        this.$bgUnderlay = $bgUnderlay;
    }

    changeBgImg() {
        let currentImgNum = parseInt(this.$bg.attr('data-bg-img-num'));
        let nextImgNum = currentImgNum + 1;

        if (nextImgNum > this.maxBgNum) {
            nextImgNum = 1;
        }

        this.$bg.attr('data-bg-img-num', nextImgNum);

    }

    show(cb) {
        if (this.isOpen) {
            if (cb) {
                return cb();
            } else {
                return false;
            }
        }
        this.isOpen = true;
        this.$toggler.addClass('b-burger--open');
        document.body.classList.add("l-body--main-menu-open");
        document.body.classList.remove("l-body--main-menu-close");
        TweenMax.killTweensOf([this.$bg, this.$list]);
        this.$modal.modal("show");

        if (cb) cb();
    }

    hide(cb) {
        if (!this.isOpen) {
            if (cb) {
                return cb();
            } else {
                return false;
            }
        }
        this.isOpen = false;
        TweenMax.killTweensOf([this.$bg, this.$list]);
        let tl = new TimelineMax();
        this.$toggler.removeClass('b-burger--open');
        document.body.classList.remove("l-body--main-menu-open");
        document.body.classList.add("l-body--main-menu-close");
        tl
            .set(this.$bg, {"animation-play-state": "paused"})
            .addLabel("animeStart")
            .to(this.$list, 0.13, { x: "-10%", opacity: "0" }, "animeStart")
            .to(this.$bgUnderlay, 0.3, { width: "0%" }, "animeStart")
            .to(this.$bg, 0.3, { width: "0%", delay: 0.08 }, "animeStart")

            .call(() => {
                this.$modal.modal("hide");
                this.changeBgImg();
            })
            .set([this.$list, this.$bg], { clearProps: "all" })
            .set(this.$list, { className: "-=b-menu__inner--open" })
            .set(this.$list.find(".b-menu__leaf--active"), { className: "-=b-menu__leaf--active" })
            .set(this.$bg, {className: "-=b-menu__bg--zooming"});



        if (cb) cb();
    }

    toggle() {
        if (this.isOpen) {
            this.hide();
        } else {
            this.show();
        }
    }

    openSubList($parentLeaf, $submenu) {
        if ($parentLeaf.hasClass('b-menu__leaf--active')) return false;

        let $list = this.$list;


        let tl = new TimelineMax();
        if (getWindowSizeBreakpoint() === "xs") {
            // SMALL WINDOW
            let submenuHeight = $submenu.actual('height');
            //alert(submenuHeight);

            if ($list.hasClass('b-menu__inner--open')) {
                // SOME SUBLIST ALREADY ACTIVE (on small devive)
                let $activeLeaf = $list.find('.b-menu__leaf--active:first');
                let $activeSubmenu = $activeLeaf.find('.b-menu__submenu:first');

                tl
                    .to($activeSubmenu, 0.15, { opacity: "0", height: "0px" })
                    .set($activeLeaf, { className: "-=b-menu__leaf--active" })
                    .set($parentLeaf, { className: "+=b-menu__leaf--active" })
                    .fromTo($submenu, 0.3, {
                        height: "0px",
                        opacity: "0"
                    }, {
                        opacity: "1",
                        height: submenuHeight,
                    })
                    .set([$submenu, $activeSubmenu], { clearProps: "all" });
            } else {
                // NO ACTIVE SUBLIST (on small device)
                tl
                    .set($list, { className: "+=b-menu__inner--open" })
                    .set($parentLeaf, { className: "+=b-menu__leaf--active" })
                    .addLabel("animeStart")
                    .fromTo($submenu, 0.3, { height: "0px" }, { height: submenuHeight }, "animeStart")
                    .from($submenu, 0.2, { opacity: "0", delay: 0.2 }, "animeStart")
                    .set($submenu, { clearProps: "all" });
            }
        } else {
            // BIG WINDOWS
            if ($list.hasClass('b-menu__inner--open')) {
                // SOME SUBLIST ALREADY ACTIVE
                let $activeLeaf = $list.find('.b-menu__leaf--active:first');
                let $activeSubmenu = $activeLeaf.find('.b-menu__submenu:first');
                let leafShiftByOrder = $activeLeaf.index() < $parentLeaf.index() ? 40 : -40;
                tl
                    .to($activeSubmenu, 0.3, { y: (-1 * leafShiftByOrder + "%"), opacity: "0" })
                    .set($activeLeaf, { className: "-=b-menu__leaf--active" })
                    .set($parentLeaf, { className: "+=b-menu__leaf--active" })
                    .from($submenu, 0.3, { y: (leafShiftByOrder + "%") })
                    .set([$activeSubmenu, $submenu], { clearProps: "all" });
            } else {
                // NO ACTIVE SUBLIST
                let currentListMargin = $list.css("left");
                tl
                    .set($list, { className: "+=b-menu__inner--open" })
                    .set($parentLeaf, { className: "+=b-menu__leaf--active" })
                    .addLabel("animeStart")
                    .from($list, 0.4, { left: currentListMargin }, "animeStart")
                    .from($submenu, 0.4, { x: "-20%", opacity: 0, delay: 0.1 }, "animeStart")
                    .set([$submenu, $list], { clearProps: "all" });
            }
        }
    }
}