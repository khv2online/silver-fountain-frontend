$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Поле заполнено не верно"
);
class InteractiveForm {
    constructor(opts) {
        this.$form = $(opts.el).eq(0);

        let $form = this.$form;
        let $submit = $form.find('input[type="submit"]');

        $submit.on('click', function(e) {
            try {
                let formId = $form.attr("id");
                let data;

                if (formId === "tradeinForm") {
                    data = "form_submit_tradein";
                } else if (formId === "hypothecForm") {
                    data = "form_submit_mortage";
                } else if (formId === "flatOrderForm") {
                    data = "form_submit_flat";
                } else {
                    data = "form_submit_callback";
                }
                console.log("send event to yandex: ", data);
                yaCounter45266157.reachGoal(data);
            } catch (err) {
                console.error("Error!!! Can't send statistics\n", err);
            }
        });

        if ($form.data("action")) {
            $form.prop('action', $form.data("action"));
        }

        $form.find('[name="phone"]').inputmask({
            mask: "8 999 999-99-99",
            showMaskOnHover: false,
        });

        $form.find('[name="captcha"]').inputmask({
            mask: "9999",
            showMaskOnHover: false
        });

        let validatorOpts = {
            rules: {
                phone: {
                    regex: /8\s\d\d\d\s\d\d\d\-\d\d\-\d\d/
                },

            },
            onfocusout: (el, event) => {
                $(el).valid();
            },

            onclick: false,
            focusCleanup: false,
            submitHandler: opts.submitHandler || this.standartFormHandler, //(form)=>{}
            errorPlacement: ($errorLabel, $el) => {

                if ($el.is(".b-custom-checkbox__input")) {
                    return true;
                } else {
                    $errorLabel.insertAfter($el);
                }

            },
        };

        if (opts.validatorParams) {
            $.extend(true, validatorOpts, opts.validatorParams);
        }

        this.validator = $form.validate(validatorOpts);
    }

    standartFormHandler(form) {
        let $form  = $(form);

        window.pagePreloader.show();

        $.ajax({
            url  : form.action,
            type : form.method,
            data : $form.serialize(),
        }).done((response) => {
            let errorCode = parseInt(response.code);

            if (errorCode === 0) {
                let successText = `\
                <div class="b-form-success">\
                    <i class="b-form-success__icon"></i>\
                    <div class="b-form-success__text">\
                        ${response.success}\
                    </div>\
                </div>`;

                window.requestAnimationFrame(() => {
                    try {
                        let formId = $form.attr("id");
                        let data;

                        if (formId === "tradeinForm") {
                            data = "form_tradein";
                        } else if (formId === "hypothecForm") {
                            data = "form_mortage";
                        } else if (formId === "flatOrderForm") {
                            data = "form_flat";
                        } else {
                            data = "form_callback";
                        }
                        console.log("send event to yandex: ", data);
                        yaCounter45266157.reachGoal(data);
                    } catch (err) {
                        console.error("Error!!! Can't send statistics\n", err);
                    }

                    $form
                        .parent()
                        .hide()
                        .after(successText);
                });

            } else if (errorCode === 1) {
                let validator = $form.data('validator');
                $form.find('[name="captcha"]:first').val("").trigger('focus');
                $form.find('.b-catpchagrp__img:first').trigger('click');

                validator.showErrors({
                    captcha: response.error,
                });
            }
        }).always(( /*response*/ ) => {
            window.pagePreloader.hide();
        });
    }

    destroy() {
        this.validator.destroy();
        this.$form.find('input').inputmask('remove');
    }

}
