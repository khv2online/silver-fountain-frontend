window.viewInitializers.homepage = ($newView) => {
    let videoBg = $($newView).find('.b-splashscreen__video:first').get(0);

    let $body = $(document.body);

    let breakpoint = getWindowSizeBreakpoint();

    /*= include '../chunks/homepage-slider-init.js' */

    homepagePromoSlider.stopAutoplay();

    let loadAndStartVideo = () => {
        let req = new XMLHttpRequest();
        req.open('GET', '/video/homepage-video.mp4', true);
        req.responseType = "arraybuffer";


        req.onload = function(oEvent) {

            let blob = new Blob([oEvent.target.response], { type: "video/mp4" });

            let vid = URL.createObjectURL(blob);

            videoBg.src = vid;
            videoBg.classList.remove("b-splashscreen__video--hidden");
            window.pagePreloader.unlock();
            window.pagePreloader.hide();
            videoBg.play();
        };

        req.onerror = function() {
            console.error("ERROR - can't get video file by ajax !!!!");
        };

        req.send();
    };

    let bigScreenInit = () => {
        window.pagePreloader.lock();
        $body.addClass('l-body--untochable');
        videoBg.onended = () => {
            let $wrap = $newView.find('.l-home:first');
            let $track = $wrap.find(".l-home__vertical-track:first");
            let $sliderWrap = $track.find(".l-home__view--promo:first");
            let $splashscreenWrap = $track.find(".l-home__view--splashscreen:first");
            let $splashScreen = $track.find('.b-splashscreen__inner:first');
            let $promo = $sliderWrap.find('.b-promo:first');
            let $slider = $promo.find('.b-promo__slider:first');
            let slideH = $sliderWrap.outerHeight();
            let promoH = $promo.height();
            let bottomH = $promo.find(".b-promo__bottom:first").height();

            let promoBlockMoveVal = (slideH - promoH) / 2 + (promoH - bottomH) - 22;

            let tl = new TimelineLite();

            tl
                .set($promo, { y: -promoBlockMoveVal })
                .set($slider, { opacity: 0 })
                .addLabel("step2")
                .to($track, 0.4, { top: "-188px" }, "step2")
                .to($splashScreen, 0.4, { y: "94px" }, "step2")
                .set($splashScreen, { className: "b-splashscreen__inner b-splashscreen__inner--step2" })
                .set($track, { className: "l-home__vertical-track l-home__vertical-track--step2" })
                .set([$track, $splashScreen], { clearProps: "all" })
                .addLabel("step3")
                .to($track, 0.4, { top: "-100%" }, "step3+=1.1")
                .to($promo, 0.4, { y: 0 }, "step3+=1.1")
                .to($slider, 0.4, { opacity: 1 }, "step3+=1.1")
                .to($splashScreen, 0.4, { y: "50%" }, "step3+=1.1")
                .set($("#ft"), { className: "-=b-footer--hidden" }, "step3+=1.1")
                .set($("#hd"), { className: "-=b-header--hidden" }, "step3+=1.1")
                .set($wrap, { className: "+=l-home--step3" })
                .set($track, { className: "+=l-home__vertical-track--step3" })
                .set($splashscreenWrap, { className: "+=hidden-sm" })
                .set([$track, $wrap, $splashscreenWrap, $promo, $slider], { clearProps: "all" })
                .call(() => {
                    $body.removeClass('l-body--untochable');
                    $sliderWrap.find('.b-homepage-underlay-text:first').removeClass('b-homepage-underlay-text--hidden');
                    homepagePromoSlider.startAutoplay();
                });
        };

        loadAndStartVideo();

    };




    let tabletScreenInit = () => {
        window.pagePreloader.lock();
        $body.addClass('l-body--untochable');
        videoBg.onended = () => {
            let $wrap = $newView.find('.l-home:first');
            let $track = $wrap.find(".l-home__vertical-track:first");
            let $splashscreenWrap = $track.find(".l-home__view--splashscreen:first");

            let tl = new TimelineLite();

            tl
                .addLabel("start")
                .to($track, 0.6, { top: `-${$splashscreenWrap.height()}px` }, "start")
                .to($splashscreenWrap, 0.4, { opacity: 0.4 }, "start")
                .set($wrap, { className: "+=l-home--step3" })
                .set($track, { className: "+=l-home__vertical-track--step3" })
                .set($splashscreenWrap, { className: "+=hidden-sm" })
                .set([$track, $wrap, $splashscreenWrap], { clearProps: "all" })
                .addLabel("glideComplete")
                .set($("#ft"), { className: "-=b-footer--hidden" }, "glideComplete")
                .set($("#hd"), { className: "-=b-header--hidden", delay: -0.15 }, "glideComplete")
                .call(() => {
                    $body.removeClass('l-body--untochable');
                    $wrap.find('.b-homepage-underlay-text:first').removeClass('b-homepage-underlay-text--hidden');

                });
        };

        loadAndStartVideo();


    };


    let smallScreenInit = () => {
        let $wrap = $newView.find('.l-home:first');
        let $track = $wrap.find(".l-home__vertical-track:first");
        let $splashscreenWrap = $track.find(".l-home__view--splashscreen:first");



        let tl = new TimelineLite();

        tl

            .addLabel("hideSplash")
            .set($wrap, { className: "+=l-home--step3" }, "hideSplash")
            .set($track, { className: "+=l-home__vertical-track--step3" }, "hideSplash")
            .set($splashscreenWrap, { className: "+=hidden-sm" }, "hideSplash")
            .set([$track, $wrap, $splashscreenWrap], { clearProps: "all" }, "hideSplash")
            .set($("#ft"), { className: "-=b-footer--hidden" }, "hideSplash")
            .set($("#hd"), { className: "-=b-header--hidden" }, "hideSplash")
            .call(() => {
                $wrap
                    .find('.b-homepage-underlay-text:first')
                    .removeClass('b-homepage-underlay-text--hidden');
            });

    };

    if (breakpoint === "md" || breakpoint === "lg") {
        bigScreenInit();
    } else if (breakpoint === "sm") {
        tabletScreenInit();
    } else if (breakpoint === "xs") {
        smallScreenInit();
        
        let $window = $(window);

        $window.on('touchend.tpmScrollJacking', () => {
            
            let currentScroll = $window.scrollTop();
            let winH = $window.height();
            
            if (currentScroll < winH / 4) {
                $("html, body").animate({
                    scrollTop: 0,
                }, 300);
            } else if (currentScroll > winH) {
                $window.off('.tpmScrollJacking');
            } else {
                $newView.find('.b-splashscreen__more:first').trigger('click');
            }
        });
    }

    $newView.find('.b-splashscreen__more:first').on('click', (event) => {
        event.preventDefault();
        $("html, body").animate({
            scrollTop: $(".b-promo:first").offset().top,
        }, 300);
        $(window).off('.tpmScrollJacking');
    });

    $newView.one('replace', () => {
        $(window).off(".homepageEvents").off(".tpmScrollJacking");
        homepagePromoSlider.destroy();
        titlesMiniSlider.destroy();
        $body.removeClass('l-body--untochable');
    });
};