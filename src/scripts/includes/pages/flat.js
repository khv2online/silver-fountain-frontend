window.viewInitializers.flat = ($newView) => {

    let orderFlatInteractiveForm = new InteractiveForm({
        el: '#flatOrderForm',
    });

    $newView.one('replace', (/*event*/) => {
        orderFlatInteractiveForm.destroy();
    });

    $newView.find('.flat_planTypes_btn').click(function() {
        let flatType = $(this).data('flat-type');
        let $currentFlat = $newView.find('.flat_plan').find('#' + flatType);

        $newView.find('.flat_scheme_img').hide();
        $currentFlat.show();
        $newView.find('.flat_planTypes_btn').removeClass('flat_planTypes_btn-active');
        $(this).toggleClass('flat_planTypes_btn-active');
    });

    let flatOrderForm = $('#flatOrderForm');
    let callMeCont = $('.call_me');
    let callMeHead = $('.call_me_head');
    let callBtn = flatOrderForm.find('input[type="submit"]');
    callBtn.on('click', (event)=> {
       event.preventDefault();
        let phoneInput = flatOrderForm.find('input[type="tel"]');
        let acceptCheckboxInCallmeForm = flatOrderForm.find('#acceptCheckboxInCallmeForm');
        let phoneInputValid = phoneInput.attr('aria-invalid');
        let acceptCheckboxInCallmeFormValid = acceptCheckboxInCallmeForm.attr('aria-invalid');

        if (phoneInputValid === 'false' && acceptCheckboxInCallmeFormValid === 'false') {
            let phoneInputVal = phoneInput.val();
            let phoneNumber = phoneInputVal.replace( /-/g, "" ).replace(/\s/g, '');
            ComagicWidget.sitePhoneCall( { phone: phoneNumber }, function(resp) { if(resp.success !== undefined && resp.success) {
                flatOrderForm.hide();
                callMeHead.hide();
                callMeCont.append('<div class="call_me_success">Спасибо, наши менеджеры свяжутся с Вами в ближайшее время!</div>');
            } } );
        }


    });

    $('[data-fancybox="inline"]').fancybox({
        afterClose: function( instance, slide ) {
            flatOrderForm.find('input[type="tel"]').val('');
            $('#acceptCheckboxInCallmeForm').prop('checked', 0);
            flatOrderForm.show();
            callMeHead.show();
            $('.call_me_success').remove();

            console.log('close')
        }
    });
};