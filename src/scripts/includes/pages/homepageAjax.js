window.viewInitializers.homepageAjax = ($newView) => {


    /*= include '../chunks/homepage-slider-init.js' */

    $newView.one('replace', (event) => {
    	$(window).off(".homepageEvents");
        homepagePromoSlider.destroy();
        titlesMiniSlider.destroy();
        homepagePromoSlider = titlesMiniSlider = null;
    });
};