window.viewInitializers.actions = ($newView) => {
    let $actionsSlider = $newView.find('.b-actsly:first');
    let actionsSliderBlock = $actionsSlider.find('.b-actsly__inner:first').get(0);
    let actionsSliderInstance = new IScroll(actionsSliderBlock, {
        scrollbars: false,
        snap: ".b-actsly__leaf",
        scrollX: true,
        scrollY: false,
        fadeScrollbars: false,
        interactiveScrollbars: true,
        resizeScrollbars: false,
        click: true,
        indicators: {
            el: $newView.find(".time_slide:first").get(0),
            fade: false,
            ignoreBoundaries: false,
            interactive: true,
            listenX: true,
            listenY: false,
            resize: false,
            shrink: false,
            speedRatioX: 0,
            speedRatioY: 0,
        }
    });


    $actionsSlider.find('.b-actsly__arrow').on('click', (event)=> {
        event.preventDefault();
        if (event.target.classList.contains("b-actsly__arrow--next")) {
            actionsSliderInstance.next();
        }else{
            actionsSliderInstance.prev();
        }
    });

    
    //TODO: handle this comment
    /*$(window).on('resize orientationchange', _.debounce( (event) => {
        event.preventDefault();
        actionsSliderInstance.destroy();
    }, 100));*/

    $newView.one('replace', () => {
        actionsSliderInstance.destroy();
        actionsSliderInstance = actionsSliderBlock = null;
    });
};
