window.viewInitializers.commercial = ($newView) => {
    var swiper = new Swiper("[data-slider-simple]", {
        slidesPerView: 3,
        loop: true,
        nextButton: $("[data-simple-slider-next]"),
        prevButton: $("[data-simple-slider-prev]"),
        spaceBetween: 0,
        breakpoints: {
            768: {
                slidesPerView: 1,
            },
        },
        onlyExternal: true,
    });

    var gallerySwiper = new Swiper("[data-slider-gallery]", {
        slidesPerView: 1,
        loop: false,
        spaceBetween: 0,
        nextButton: $("[data-gallery-slider-next]"),
        prevButton: $("[data-gallery-slider-prev]"),
    });

    $newView.one("replace", (/*event*/) => {
        swiper.destroy();
        gallerySwiper.destroy();
    });
};
