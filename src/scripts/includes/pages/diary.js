window.viewInitializers.diary = ($newView) => {

    let sliders = [];
    let $outerSliderBlock = $newView.find('.b-diary-outersly:first');

    var player;
    var url;
    var urlParams = '?rel=0&amp;controls=0&amp;showinfo=0&version=3&enablejsapi=1&autoplay=1';

    $outerSliderBlock.find('.b-diary-outersly__slide').each((index, el) => {
        let $el = $(el);
        let $innerSlider = $el.find('.b-diary-innersly:first');
        let diaryInnerSlyInstance = new Swiper($innerSlider, {
            centeredSlides: true,
            speed: 500,
            nextButton: $el.find(".dynamics_foto_count_controls:first").find('.next'),
            prevButton: $el.find(".dynamics_foto_count_controls:first").find('.prev'),
            pagination: $el.find(".count:first"),
            paginationType: 'fraction',
            slidesPerView: 1,
            paginationFractionRender: (swiper, currentClassName, totalClassName) => {
                return '<span class="' + currentClassName + '"></span>' +
                    ' из ' +
                    '<span class="' + totalClassName + '"></span>';
            },
            onSlideChangeStart: (swiper) => {
                let $slider       = swiper.container;
                let $sliderParent = $slider.parent().parent();
                let $titleBlock   = $sliderParent.find(".dynamics_name_foto:first");
                let $authorBlock  = $sliderParent.find(".dynamics_autor_foto:first");
                let currentSlide  = swiper.slides[swiper.realIndex];

                if ($(currentSlide).find('template').length > 0) {
                    let templates    = currentSlide.getElementsByTagName('template');
                    let titleMarkup  = templates[0].innerHTML;
                    let authorMarkup = templates[1].innerHTML;

                    $titleBlock.html(titleMarkup);
                    $authorBlock.html(authorMarkup);

                    if (url) {
                        url = url + urlParams.split("&autoplay=1");

                        $('.yvideo').attr('src', url);
                    }
                }

            },
            onInit: function(swiper) {
                this.onSlideChangeStart(swiper);
            },
            lazyLoadingInPrevNext: true,
            lazyLoading: true,
            preloadImages: true,
        });

        sliders.push(diaryInnerSlyInstance);
    });

    let $monthTitle = $newView.find('.month.active:first');

    let outerSliderInstance = new Swiper($outerSliderBlock, {
        effect: "fade",
        fade: {
            crossFade: true,
        },

        onlyExternal: true,
    });

    let monthsSliderInstance = new Swiper($newView.find('.b-months-slider:first'), {
        direction: "vertical",
        slidesPerView: 3,
        nextButton: $newView.find(".dynamics_month_controls:first").find('.next'),
        prevButton: $newView.find(".dynamics_month_controls:first").find('.prev'),
        centeredSlides: true,
        onSlideChangeStart: (swiper) => {
            $monthTitle.html(swiper.slides[swiper.realIndex].innerHTML);

            if (url) {
                url = url + urlParams.split("&autoplay=1");

                $('.yvideo').attr('src', url);
            }
        },
        onInit: function(swiper) {
            this.onSlideChangeStart(swiper);
        },
        onlyExternal: true,
        slideToClickedSlide: true,
    });

    monthsSliderInstance.params.control = outerSliderInstance;

    sliders.push(outerSliderInstance, monthsSliderInstance);

    let $circleWrap = $newView.find("#diaryProgressCircle");

    let getCircleRadius = () => {
        return Math.floor($circleWrap.width() / 2);
    };

    let $timelineBullet = $newView.find('.dynamics_timeline_count:first');
    setTimeout(() => {
        $timelineBullet.removeClass('dynamics_timeline_count--invisible');
    }, 1000);

    let circleInst = Circles.create({
        id: 'diaryProgressCircle',
        radius: getCircleRadius(),
        value: 10,
        maxValue: 100,
        width: 2,
        text: false,
        colors: ['#697493', '#fff'],
        duration: 1000,
        wrpClass: 'b-diary-circle',
        styleWrapper: true,
    });

    $(window).on('resize.diaryPageEvents orientationchange.diaryPageEvents', _.debounce(() => {
        circleInst.updateRadius(getCircleRadius());
    }, 100));

    var broadcastSlider = new Swiper('.broadcast-slider', {
        speed: 400,
        spaceBetween: 20,
        observer: true,
        observeParents: true,
        resistance: true,
        resistanceRatio: 0,
        nextButton: '.broadcast-slider-controls__next',
        prevButton: '.broadcast-slider-controls__prev',
        onSlideChangeEnd: function(swiper) {
            var activeIndex = swiper.activeIndex;
            $(broadcastSliderThumbs.slides).removeClass('is-selected');
            $(broadcastSliderThumbs.slides).eq(activeIndex).addClass('is-selected');
            broadcastSliderThumbs.slideTo(activeIndex, 400, false);
        }

    });

    var broadcastSliderThumbs = new Swiper('.broadcast-slider-thumbs', {
        spaceBetween: 10,
        freeMode: true,
        resistance: true,
        resistanceRatio: 0,
        centeredSlides: false,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        onClick: function(swiper, event) {
            var clicked = swiper.clickedIndex;
            swiper.activeIndex = clicked;
            swiper.updateClasses();
            $(swiper.slides).removeClass('is-selected');
            $(swiper.clickedSlide).addClass('is-selected');
            broadcastSlider.slideTo(clicked, 400, false);

        },
        observer: true,
        observeParents: true,
    });


    sliders.push(broadcastSlider, broadcastSliderThumbs);

    let $broadCastFrames = $newView.find('.broadcast-iframe');

    let changeSizeBroadCastFrame = () => {
        $broadCastFrames.each((index, el) => {
            let $brFrame = $(el);
            let ww = $brFrame.width();
            let hw = Math.round(ww / 1.777777777777778);
            let newFrameSrc = $brFrame.attr('src').split("?")[0] + '?' + ww + 'x' + hw;
            $brFrame.attr('src', newFrameSrc);
        });

    };

    $newView.find(".broadcast-link").fancybox({
        afterShow: function() {
            changeSizeBroadCastFrame();

            if (url) {
                url = url + urlParams.split("&autoplay=1");

                $('.yvideo').attr('src', url);
            }
        }
    });

    $(window).on("resize.diaryPageEvents", _.debounce(() => {
        if (document.querySelector(".fancybox-is-open")) {
            changeSizeBroadCastFrame();
        }

    }, 100));


    $newView.one('replace', () => {
        sliders.forEach((sld) => {
            sld.destroy();
        });

        $(window).off(".diaryPageEvents");
    });

    $(document).on('click', '[data-video]', function(e) {
        let $this = $(this);

        url = $this.data('video') + urlParams;

        $this.replaceWith('<div class="embed-responsive"><iframe id="diary-video" class="b-diary-innersly__img yvideo" width="100%" height="100%" src="' + url + '"autoplay; frameborder="0" allow="encrypted-media" allowfullscreen></iframe></div>');
    });
}
