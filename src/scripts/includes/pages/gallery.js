window.viewInitializers.gallery = ($newView) => {

    let $galleryButtons = $newView.find('.gallery_popup_trigger');

    $galleryButtons.on('click', (event) => {
        event.preventDefault();
        $.ajax({
                url: event.target.dataset.href,
                dataType: 'json',
            })
            .done((items) => {
                $.fancybox.open(items, {
                    loop : true,
                    slideShow : {
                        autoStart : true,
                        speed     : 4000,

                    },
                });
            })
            .fail(function() {
                console.error("Can't get gallery data from server!");
            });

        return false;
    });



    /*$newView.one('replace', (event) => {

    });*/
};