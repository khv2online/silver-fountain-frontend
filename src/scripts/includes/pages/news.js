window.viewInitializers.news = ($newView) => {


if ($(window).width() > 1024) {
	$('.l-body--news .news_l_item_name').ellipsis({
	    row: 4
	});
}
else if ($(window).width() <= 1024 && $(window).width() >= 768) {
	$('.l-body--news .news_l_item_name').ellipsis({
	    row: 3
	});
}
else if ($(window).width() < 768) {
	$('.l-body--news .news_l_item_name').ellipsis({
	    row: 2
	});
}

};