window.viewInitializers.contacts = ($newView) => {
    const MAP_HORIZONTAL_SHIFT = 0.001;

    ymaps.ready(init);

    function init() {
        let buildingLocation = {
            lat: 55.808775,
            lng: 37.646775,
        };

        let polygonCoords = [
            [55.81007, 37.64956],
            [55.809774, 37.649762],
            [55.809831, 37.650121],
            [55.809303, 37.65048],
            [55.809134, 37.649833],
            [55.808811, 37.649586],
            [55.808594, 37.650203],
            [55.807939, 37.649666],
            [55.80791, 37.649789],
            [55.807851, 37.64973],
            [55.807718, 37.650175],
            [55.807828, 37.650288],
            [55.807692, 37.650749],
            [55.807784, 37.650835],
            [55.807725, 37.651006],
            [55.806757, 37.649925],
            [55.806525, 37.649356],
            [55.807304, 37.646382],
            [55.807815, 37.646213],
            [55.807827, 37.645907],
            [55.807923, 37.64591],
            [55.807929, 37.646253],
            [55.808093, 37.646191],
            [55.808722, 37.646293],
            [55.808734, 37.646143],
            [55.808974, 37.646189],
            [55.808986, 37.646355],
            [55.809682, 37.6465],
        ];

        var myMap = new ymaps.Map("map", {
            center: [buildingLocation.lat, buildingLocation.lng - MAP_HORIZONTAL_SHIFT],
            zoom: 17,
            controls: [],
        });

        function setCenter(lat, lng) {
            myMap.setCenter([lat, lng], 17);
        }

        function addMarker(options, template) {
            var placemark = new ymaps.Placemark(
                [options.position[0], options.position[1]],
                {
                    hintContent: options.title,
                    balloonContent: template != undefined ? template : options.title,
                },
                {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: "default#image",
                    // Своё изображение иконки метки.
                    iconImageHref: options.icon.url,
                    // Размеры метки.
                    iconImageSize: options.icon.size,
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [(-1 * options.icon.size[0]) / 2, -1 * options.icon.size[1]],
                }
            );

            myMap.geoObjects.add(placemark);
        }

        function addPolygon() {
            var myPolygon = new ymaps.Polygon(
                [
                    // Указываем координаты вершин многоугольника.
                    // Координаты вершин внешнего контура.
                    polygonCoords,
                ],
                {
                    // Описываем свойства геообъекта.
                    // Содержимое балуна.
                    hintContent: "",
                },
                {
                    // Задаем опции геообъекта.
                    // Цвет заливки.
                    fillColor: "rgba(255, 255, 255, 0)",
                    // Цвет обводки.
                    strokeColor: "#ccc",
                    // Ширина обводки.
                    strokeWidth: 5,
                }
            );

            // Добавляем многоугольник на карту.
            myMap.geoObjects.add(myPolygon);
        }

        function addRegions($item, options) {
            myMap.setZoom(options.zoom);
            myMap.setCenter(options.position);

            $item.find("[data-map-regions]").each(function() {
                let $this = $(this);
                let regionOptions = $this.data("map-regions");
                let position = regionOptions.position;
                let squareLayout = ymaps.templateLayoutFactory.createClass(
                    '<div class="placemark_layout_container"><div class="square_layout">' +
                        regionOptions.title +
                        "</div></div>"
                );
                let template = $this.find("template").html();

                let regObject = new ymaps.Placemark(
                    [position[0], position[1]],
                    {
                        balloonContentBody: template,
                    },
                    {
                        iconLayout: squareLayout,
                        // Описываем фигуру активной области "Прямоугольник".
                        iconShape: {
                            type: "Rectangle",
                            // Прямоугольник описывается в виде двух точек - верхней левой и нижней правой.
                            coordinates: [[-90, -66], [90, 0]],
                        },
                    }
                );

                myMap.geoObjects.add(regObject);
            });
        }

        $("#accordion").on("show.bs.collapse", function(event) {
            let $item = $(event.target);
            let options = $item.data("map");
            let id = $item.attr("id").split("_")[1];

            myMap.geoObjects.removeAll();

            if (id == 4) {
                addRegions($item, options);

                return;
            }

            setCenter(options.position[0], options.position[1]);
            addMarker(options);

            if (id == 1) {
                addPolygon();
            }
        });

        $("[data-map-regions]").on("click", function(e) {
            let $this = $(this);
            let options = $this.data("map-regions");
            let template = $this.find("template").html();

            console.log(options);

            e.preventDefault();

            myMap.geoObjects.removeAll();

            setCenter(options.position[0], options.position[1]);

            addMarker(options, template);
        });

        let helpUsFrom = new InteractiveForm({
            el: "#helpUsForm",
        });

        $newView.one("replace", (event) => {
            myMap.destroy();
            helpUsFrom.destroy();
        });

        $('a[href="#contacts_1"]').trigger("click");
    }

    $newView.find(".contacts_list .more").fancybox();
};
