window.viewInitializers.filters = ($newView) => {
    if (!window.location.hash) {
        history.replaceState(undefined, undefined, "#section-1");
    }

    window.pagePreloader.lock();
    const CURRENT_PATH = window.location.pathname;
    const preserveFilterState = ($form) => {
        let seriazedStates = $form.serializeObject();
        for (let name in seriazedStates) {
            if (_.includes(["view_direction[]", "rooms[]", "plantype[]", "sections[]"], name)) {
                let arr = seriazedStates[name];
                for (let i = arr.length - 1; i >= 0; i--) {
                    arr[i] = parseInt(arr[i]);
                }
            } else if (
                _.includes(
                    ["floor_to", "floor_from", "area_to", "area_from", "discount", "apartament", "only_flats"],
                    name
                )
            ) {
                seriazedStates[name] = parseInt(seriazedStates[name]);
            }
        }

        localStorage.setItem(`savedFlatFilterStates${CURRENT_PATH}`, JSON.stringify(seriazedStates));
        return seriazedStates;
    };

    const restoreFilterState = ($form) => {
        let savedStates = JSON.parse(localStorage.getItem(`savedFlatFilterStates${CURRENT_PATH}`));
        if (!savedStates) return false;

        for (let name in savedStates) {
            let value = savedStates[name];
            if (_.isArray(value)) {
                // set checkboxes values
                $form.find(`input[name='${name}']`).each((index, el) => {
                    el.checked = _.includes(value, parseInt(el.value)) ? true : false;
                });
            } else {
                // single fields
                let $inp = $form.find(`input[name='${name}']:first`);
                if ($inp.attr('type') === "checkbox") {
                    $inp.prop("checked", true);
                }else{
                    $inp.val(value);
                }

                console.log($form.find(`input[name='${name}']:first`));
            }
        }

        {
            //restore svg scheme active sections
            let $activeSectionsInps = $form.find(".b-filter-scheme__checkbox");
            let $schemePolygons = $form.find(".b-filter-scheme__polygon");

            $activeSectionsInps.each((index, el) => {
                if (el.checked) {
                    $schemePolygons.eq(index).addClass("b-filter-scheme__polygon--active");
                }
            });
        }
    };

    let $filterForm = $newView.find("#pickupFlatFilterForm");

    restoreFilterState($filterForm);

    const setRangeInputsValues = (event, ui) => {
        let $inputs = $(event.target.getElementsByTagName("input"));

        for (var i = 0; i < 2; i++) {
            let $inp = $inputs.eq(i);
            let oldVal = $inp.val();
            let newVal = ui.values[i];

            if (oldVal != newVal) {
                $inp.val(newVal).trigger("change");
            }
        }
    };

    $(".checkboxradio input").checkboxradio({
        icon: false,
    });
    let $simpleSliders = $("#squareSlider, #floorSlider");

    $simpleSliders.each(function(index, el) {
        let $el = $(el);
        let opts = $el.data("opts");
        let $inputs = $el.find("input");
        let currentMinMax = [$inputs.eq(0).val(), $inputs.eq(1).val()];
        $(el).slider({
            range: true,
            values: currentMinMax,
            min: opts.min,
            max: opts.max,
            slide: setRangeInputsValues,
        });
    });

    $($filterForm.get(0).elements).on(
        "change",
        _.debounce((event) => {
            event.preventDefault();
            $filterForm.trigger("submit");
        }, 500)
    );

    let $sectionsSwiperArrows = $newView.find(".choose_result_section_controls:first").find("a");

    let $sectionsSwipebox = $newView.find(".choose_result_section_item_inner:first");
    let sectionsSwiperInst = new Swiper($sectionsSwipebox, {
        slidesPerView: "auto",
        spaceBetween: 10,
        nextButton: $sectionsSwiperArrows[1],
        prevButton: $sectionsSwiperArrows[0],
        centeredSlides: true,
        initialSlide: 2,
        slideActiveClass: "b-sections-slider__slide--active",
        slideNextClass: "b-sections-slider__slide--next",
        slidePrevClass: "b-sections-slider__slide--prev",
        onSetTranslate: function(swiper, translate) {
            swiper.container.find(".swiper-wrapper:first").css("transform", "translate(" + translate + "px, 0)");
        },
        onSlideChangeEnd: (swiper) => {
            history.replaceState(undefined, undefined, "#section-" + (swiper.realIndex + 1));
        },
        breakpoints: {
            768: {
                slidesPerView: 1,
                initialSlide: 0,
                autoHeight: true,
            },
        },
        onInit: () => {
            let lastFlatFilterTabHref = localStorage.getItem("lastFlatFilterTab");
            if (lastFlatFilterTabHref) {
                $newView
                    .find(".choose_result_tab:first")
                    .find(`a[href="${lastFlatFilterTabHref}"]:first`)
                    .trigger("click");
            }
        },
    });

    let lazyLoadIst = new LazyLoad({
        elements_selector: ".lazy-load",
    });

    $newView
        .find(".choose_result_tab:first")
        .find("a")
        .on("shown.bs.tab", (event) => {
            let $link = $(event.currentTarget);
            let tabHref = $link.attr("href");
            if (tabHref === "#section_result") {
                sectionsSwiperInst.update(true);
                if (sectionsSwiperInst.isUpdatedThenHidden === true) {
                    sectionsSwiperInst.slideTo(0, 0, false);
                    sectionsSwiperInst.isUpdatedThenHidden = false;
                }
            } else if (tabHref === "#plit_result") {
                lazyLoadIst.update();
            }

            localStorage.setItem("lastFlatFilterTab", tabHref);
        });

    $newView.find(".close_filtr,.open_filtr").on("click", (event) => {
        event.preventDefault();
        $newView.find(".choose_filtr:first").toggleClass("open");
    });

    let tplStr = `
        /*=require ../chunks/section_slide_tpl.html */
    `;

    tplStr = tplStr.replace(/\s{2,}/g, "");

    let chessSchemesSlideTpl = _.template(tplStr);

    const renderFlats = (data) => {
        let sections = [];
        let allFlats = [];
        let viewDirections = {
            "4": { fullName: "на Парк Сокольники", shortName: "на Сокольники" },
            "5": { fullName: "на внутреннее пространство двора", shortName: "во двор" },
            "6": { fullName: "на липовую аллею", shortName: "на липовую аллею" },
            "7": { fullName: "на город", shortName: "на город" },
        };

        data.houses.forEach((h) => {
            sections.push(h.sections);
        });

        sections = _.flatten(sections);

        sections.forEach((s) => {
            let floors = s.floors;
            floors.forEach((f) => {
                if (f.flats.length) {
                    f.flats.forEach((flat) => {
                        flat.numberOfFloors = floors[0].floor;
                    });
                    allFlats.push(f.flats);
                }
            });
        });

        allFlats = _.flatten(allFlats);

        // if (window.location.href.indexOf(".html")) {
        //     console.warn("rewrite flats links for dev enviroment usage...");
        //     allFlats.forEach((flat) => {
        //         flat.href = "/flat-single.html";
        //     });
        // }

        let savedFilterStates = JSON.parse(localStorage.getItem(`savedFlatFilterStates${CURRENT_PATH}`));

        let allActiveFlats = _.filter(allFlats, (flat) => {
            let testResult = true;
            if (
                flat.status !== "sale" ||
                flat.area < savedFilterStates.area_from ||
                flat.area > savedFilterStates.area_to ||
                flat.floor < savedFilterStates.floor_from ||
                flat.floor > savedFilterStates.floor_to ||
                (savedFilterStates.discount && flat.discount !== savedFilterStates.discount) ||
                (savedFilterStates.apartament !== savedFilterStates.only_flats &&
                    savedFilterStates.apartament &&
                    flat.apartament !== savedFilterStates.apartament) ||
                (savedFilterStates.apartament !== savedFilterStates.only_flats &&
                    savedFilterStates.only_flats &&
                    flat.apartament) ||
                (savedFilterStates["rooms[]"] && !_.includes(savedFilterStates["rooms[]"], flat.rooms)) ||
                (savedFilterStates["plantype[]"] &&
                    !_.includes(savedFilterStates["plantype[]"], parseInt(flat.plantype))) ||
                (savedFilterStates["view_direction[]"] &&
                    !_.intersection(flat.views_id, savedFilterStates["view_direction[]"]).length) ||
                (savedFilterStates["sections[]"] && !_.includes(savedFilterStates["sections[]"], flat.section))
            ) {
                testResult = false;
            }
            flat.filtered = testResult;
            return testResult;
        });

        {
            // insert chess schemes into the slider
            let slidesForSectionTab = [];
            sections.forEach((s) => {
                if (!s.floors.length) return true;
                slidesForSectionTab.push(chessSchemesSlideTpl({ floors: s.floors, title: s.name, index: s.section }));
            });

            sectionsSwiperInst.removeAllSlides();
            sectionsSwiperInst.appendSlide(slidesForSectionTab);

            if (!sectionsSwiperInst.container.is(":visible")) {
                sectionsSwiperInst.isUpdatedThenHidden = true;
            } else {
                let slideToShowAfterFiltersUpdate = parseInt(window.location.hash.split("section-")[1] - 1);
                sectionsSwiperInst.slideTo(slideToShowAfterFiltersUpdate, 0, false);
            }
        }

        function splitDigits(num) {
            var str = num.toString().split(".");
            if (str[0].length >= 5) {
                str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, "$1 ");
            }
            if (str[1] && str[1].length >= 5) {
                str[1] = str[1].replace(/(\d{3})/g, "$1 ");
            }
            return str.join(".");
        }

        {
            // insert flats list

            let markupForFlatList = "";
            allActiveFlats.forEach((flat) => {
                markupForFlatList += `<tr data-href="${flat.href}" class="row b-flatlist__row ${
                    flat.discount ? "b-flatlist__row--discount" : ""
                }">
                        <td class="col col_1">${flat.name}</td>
                        <td class="col col_2">Этаж ${flat.floor} / ${flat.numberOfFloors}</td>
                        <td class="col col_3">${flat.rooms} комн.<span>( ${flat.area} м<sup>2</sup> )</span></td>
                        <td class="col col_4">${flat.area} м²</td>
                        <td class="col col_5">${flat.plantype == 2 ? "Евро" : "Классика"}</td>
                        <td class="col col_6">${
                            flat.views_id[0] ? "вид " + viewDirections[flat.views_id[0]].shortName : "-"
                        }</td>
                        <td class="col col_7">${splitDigits(flat.price)} р.</td>
                    </tr>`;
            });

            if (allActiveFlats.length <= 0) {
                markupForFlatList += `<tr class="b-filters-empty b-filters-empty--list">
                        <td>
                            Квартиры по заданным параметрам не найдены,<br/> пожалуйста измените параметры поиска.
                        </td>
                    </tr>`;
            }

            var $table = $newView.find(".choose_result_list:first");
            var $body = $table.find("[data-choose-result]");

            $body.html(markupForFlatList);

            $table.trigger("destroy").tablesorter({
                cssChildRow: "row-details",
            });

            $table.find(".b-flatlist__row").on("click", function(e) {
                let $this = $(this);
                let href = $this.data("href");

                window.location.href = href;
            });
        }

        {
            // insert flat cards
            let markupForCardsList = "";
            allActiveFlats.forEach((flat /*, index*/) => {
                markupForCardsList += `
                    <a href="${flat.href}" class="choose_result_plit_item ${
                    flat.discount ? "choose_result_plit_item--discount" : ""
                }"">
                        <div class="flat_n">${flat.name}</div>
                        <div class="flat_r">${flat.rooms} комн.</div>
                        <div class="flat_sq">${flat.area} м²</div>
                        <div class="flat_pr">${splitDigits(flat.price)} рублей</div>
                        <div class="lazy-load img" data-original="${"http://etalonsilver.ru" + flat.img}"></div>
                    </a>`;
            });

            if (markupForCardsList.length <= 0) {
                markupForCardsList += `
                <div class="b-filters-empty b-filters-empty--sections">
                    <p>
                        Квартиры по заданным параметрам не найдены,<br/> пожалуйста измените параметры поиска.
                    </p>
                </div>
                `;
            }

            $newView.find(".choose_result_plit:first").html(markupForCardsList);
            lazyLoadIst.update();
        }
    };

    $filterForm
        .on("submit", (event) => {
            event.preventDefault();
            preserveFilterState($filterForm);
            let storedFlats = JSON.parse(localStorage.getItem(`cachedFlats${CURRENT_PATH}`));

            if (storedFlats && storedFlats.actualUntil > Date.now()) {
                renderFlats(storedFlats.data);
                window.pagePreloader.unlock();
                window.pagePreloader.hide();
            } else {
                $.ajax({
                    url: $filterForm.attr("action"),
                    data: {
                        ajax: "1",
                        action: "get_flats",
                    },
                }).done((response) => {
                    localStorage.setItem(
                        `cachedFlats${CURRENT_PATH}`,
                        JSON.stringify({
                            data: response,
                            actualUntil: Date.now() + 30 * 60 * 1000, // cached for 30 minutes
                        })
                    );

                    renderFlats(response);
                    window.pagePreloader.unlock();
                    window.pagePreloader.hide();
                });
            }
            return false;
        })
        .trigger("submit");

    $newView.find(".choose_filtr_param_3 > h5").on("click", (event) => {
        event.preventDefault();
        localStorage.removeItem(`cachedFlats${CURRENT_PATH}`);
        console.info("---------- FLATS WAS REMOVED -----------");
    });

    $newView
        .find(".b-filter-scheme__svg:first")
        .find(".b-filter-scheme__polygon")
        .on("click", (event) => {
            event.preventDefault();
            let $el = $(event.target);
            $el.toggleClass("b-filter-scheme__polygon--active")
                .parents(".b-filter-scheme:first")
                .find(`.b-filter-scheme__checkbox:eq(${$el.index()})`)
                .trigger("click");
        })
        .uitooltip({
            track: true,
        });

    $newView
        .find("[data-select]")
        .select2({
            width: "calc(100% - 40px)",
            minimumResultsForSearch: -1,
        })
        .on("select2:select", function(/*event*/) {
            let $table = $newView.find(".choose_result_list");
            let column = parseInt($(this).val(), 10);
            let direction = 1; // 0 = descending, 1 = ascending
            let sort = [[column, direction]];

            if (column >= 0) {
                $table.trigger("sorton", [sort]);
            }
        });

    $newView.one("replace", (/*event*/) => {
        sectionsSwiperInst.destroy();
        lazyLoadIst.destroy();
        $simpleSliders.slider("destroy");
        $newView.find("[data-select]").select2("destroy");
    });
};
