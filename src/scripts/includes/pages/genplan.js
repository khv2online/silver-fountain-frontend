window.viewInitializers.genplan = ($newView) => {
    let mapFitInstance = new Fitblock($newView.find('.b-fitblock:first').get(0));

    $(window).on('resize.genplanPage orientationchange.genplanPage', _.debounce(() => {
        mapFitInstance.refresh();
    }, 100));

    // START: genplan houses behaviour
    $newView.find('.b-house-choose_n1').hover(
        function() {
            $newView.find('.b-house-choose_mask-n1').addClass('b-house-choose_mask-active');
        },
        function() {
            $newView.find('.b-house-choose_mask-n1').removeClass('b-house-choose_mask-active');
        });

    $newView.find('.b-house-choose_n2').hover(
        function() {
            $newView.find('.b-house-choose_mask-n2').addClass('b-house-choose_mask-active');
        },
        function() {
            $newView.find('.b-house-choose_mask-n2').removeClass('b-house-choose_mask-active');
        });
    // END: genplan houses behaviour


    $('.b-house-choose_link').click(function() {
        let houseLinkHref = $(this).data('house-link');
        window.location.href = houseLinkHref;
    });


    $newView.one('replace', () => {
        $(window).off(".genplanPage");
    });
};