window.viewInitializers.news_single = ($newView) => {

	function getSlidesNumber (el) {
		return el.slides.length;
	}

	function getCurrentSlideNumber (el) {
		return el.activeIndex + 1;
	}

	const swiperOptions = {
		speed: 400,
		freeMode: false,
		nextButton: '.l-body--news_single #bd .next',
		prevButton: '.l-body--news_single #bd .prev',
		paginationClickable: true,
		slidesPerView: 1
	};


	setTimeout(function() {
		let isInited = false;

		let swiperNewsSingle = new Swiper('.news_single_slider', {
			speed: 400,
			freeMode: false,
			nextButton: '.l-body--news_single #bd .next',
			prevButton: '.l-body--news_single #bd .prev',
			paginationClickable: true,
			slidesPerView: 1,
			onInit: function(e) {
				$('.slides_current').html(getCurrentSlideNumber(e));
				$('.slides_total').html(getSlidesNumber(e));
				isInited = true;
				if (getSlidesNumber(e) == 1) {
					$('.l-body--news_single #bd .next').hide();
					$('.l-body--news_single #bd .prev').hide();
				} 
			},
			onSlideChangeStart: function(e) {
				$('.slides_current').html(getCurrentSlideNumber(e));
			}
		});

		$newView.one('replace', (event) => {
			if (isInited) {

				try {
					swiperNewsSingle.destroy(true, true);
				}
				catch(err) {
					console.error(err);
				}
			}
		});
	},150)



};