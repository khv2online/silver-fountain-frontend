window.viewInitializers.location = ($newView) => {
    ymaps.ready(init);

    function init() {
        let metroIco       = '/img/icon_location/metro.png';
        let objectIco      = '/img/mark_logo.png';
        let allMarkers     = window.ifrasObjsMarkers;
        let buildingMarker = {
            coords: [55.809691, 37.648423],
            ico: {
                url: objectIco,
                size: [198, 102]
            }
        };
        let navActive    = 'b-map-filters__trigger--active';
        let metroMarkers = [
            {
                coords: [55.819833, 37.640912],
                title: "станция метро ВДНХ",
                ico: {
                    url: metroIco,
                    size: [46, 32]
                },
            },
            {
                coords: [55.807783, 37.638741],
                title: "станция метро Алексеевская",
                ico: {
                    url: metroIco,
                    size: [46, 32]
                },
            },
            {
                coords: [55.779407, 37.633245],
                title: "станция метро Проспект Мира",
                ico: {
                    url: metroIco,
                    size: [46, 32]
                },
            },
            {
                coords: [55.789729, 37.680941],
                title: "станция метро Сокольники",
                ico: {
                    url: metroIco,
                    size: [46, 32]
                },
            }
        ];

        var map = new ymaps.Map("locationMap", {
            center: [buildingMarker.coords[0], buildingMarker.coords[1]],
            zoom: 15,
            controls: [],
        });

        addDefault();

        function addDefault() {
            addMarker(buildingMarker);

            $.each(metroMarkers, function(i, el) {
                addMarker(el);
            });
        }

        function addMarker(options) {
            let coords = options.coords;
            let title  = options.title;
            let icon   = options.ico;

            var placemark = new ymaps.Placemark([coords[0], coords[1]], {
                hintContent: title,
                balloonContent: title,
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: icon.url,
                // Размеры метки.
                iconImageSize: icon.size,
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-1 * icon.size[0] / 2, -1 * icon.size[1]]
            });

            map.geoObjects.add(placemark);
        }

        $('[data-icons-type]').on('click', function(e) {
            let $this = $(this);
            let type  = $this.data('icons-type');

            e.preventDefault();

            map.geoObjects.removeAll();

            if ($this.hasClass(navActive)) {
                $this.removeClass(navActive);
                addDefault();

                return;
            }

            $('[data-icons-type]').removeClass(navActive);
            addDefault();

            $.each(allMarkers, function(i, el) {
                if (type === el.type) {
                    if (el.coords.lat != undefined) {
                        el.coords = [el.coords.lat, el.coords.lng];
                        el.ico    = {
                            url: el.ico,
                            size: [46, 32]
                        };
                    }

                    addMarker(el);
                }
            });

            $this.addClass(navActive);
        });

        let fitMapHeight = () => {
            let docHeight   = $("#bd").height();
            let blockHeight = $(".location_wrap:first").height();
            let $mapBlock   = $("#locationMap");

            if (docHeight <= blockHeight || getWindowSizeBreakpoint() === "xs") {
                $mapBlock.css("height", "auto");
            } else {
                let heightsDiff = docHeight - blockHeight;

                $mapBlock.height($mapBlock.height() + heightsDiff);
            }
        };

        let fitMapMask = () => {
            let $mapTop = $(".l-location__top:first");
            let $mask = $(".b-location-top-mask-bg:first");
            let w = $mapTop.width();
            let h = $mapTop.outerHeight();
            let originalTopDim = 2.580645161290323;  // 1920 / 744
            let currentTopDim = w / h;

            let isHorizontalyCutted = (currentTopDim - originalTopDim) < 0;

            if (isHorizontalyCutted) {
                TweenLite.set($mask,{
                    scale: ((h * originalTopDim) / (w/100) ) / 100,
                });
            } else {
                $mask.removeAttr('style');
            }
        };

        fitMapHeight();
        fitMapMask();

        $(window).on('resize.locationPage orientationchange.locationPage', _.debounce(() => {
            fitMapHeight();
            fitMapMask();
        }, 100));

        $newView.one('replace', () => {
            map = null;
            $(window).off(".locationPage");
        });
    }
};
