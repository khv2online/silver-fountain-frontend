window.viewInitializers.greenZoom = ($newView) => {

( () => {
    
    const swiperOptions = {
        speed: 400,
        freeMode: false,
        nextButton: '.greenzoom-content .button-next',
        prevButton: '.greenzoom-content .button-prev',
        paginationClickable: true,
        loop: false
    };

    let swiperInstance = new Swiper('.greenzoom-content .swiper-container', swiperOptions);

})();


};