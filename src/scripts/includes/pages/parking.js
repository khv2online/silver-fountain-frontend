window.viewInitializers.parking = ($newView) => {
    var parkingSlider1 = new Swiper('.parking-mask-schemes-n1', {
        speed: 400,
        spaceBetween: 0,
        nextButton: $('.parking-block1 .parking-mask-stage-n2'),
        prevButton: $('.parking-block1 .parking-mask-stage-n1'),
        resistance : true,
        resistanceRatio : 0,
        observer:true,
        observeParents: true,
        breakpoints: {
            1023: {
                spaceBetween: 20,
            },
        }
    });

    var parkingSlider2 = new Swiper('.parking-mask-schemes-n2', {
        speed: 400,
        spaceBetween: 0,
        nextButton: $('.parking-block2 .parking-mask-stage-n2'),
        prevButton: $('.parking-block2 .parking-mask-stage-n1'),
        resistance : true,
        resistanceRatio : 0,
        observer:true,
        observeParents: true,
        breakpoints: {
            1023: {
                spaceBetween: 20,
            },
        }
    });

    $( function() {
        $( ".parking-mask" ).tabs({
            active: 0
        });
    } );

    function movementBtns() {
        let windowSize = $(window).width(),
            lg = 1024 <= windowSize,
            md = windowSize <= 1023 && 768 <= windowSize,
            sm = windowSize <= 767,
            consultBtn = $('.parking-gag-content__btn'),
            parkingContentBtnsWrap = $('.parking-gag-content__btns-wrap'),
            parkingContent = $('.parking-gag-content'),
            parkingContentBtns = $('.parking-gag-content__btns-wrap'),
            parkingGag = $('.parking-gag'),
            parkingContentBtnsOldPosition = $('.parking-gag-content').find('.parking-gag-content__text').eq(-1),
            parkingContentBtnsMd = $('.parking-gag-content__btns-wrap_md');


        if ( md ) {          
            parkingContentBtns.appendTo(parkingContentBtnsMd);    
            consultBtn.appendTo(parkingContentBtnsMd);        
        } if ( sm ) {
            parkingContentBtns.appendTo(parkingGag);
            consultBtn.appendTo(parkingGag);
        } if ( lg ) {
            parkingContentBtnsWrap.insertAfter(parkingContentBtnsOldPosition);
            consultBtn.appendTo(parkingContent);
        }
    };

    
    movementBtns();
    $( window ).resize(function() {
        movementBtns();
    });


    $newView.on('replace', (event)=> {
        event.preventDefault();
        parkingSlider1.destroy();
        parkingSlider2.destroy();
    });

    $('.parking-blocks__btn').click(function(){
        var pdf = $(this).data('pdf');
        $('.parking-gag-content__btns-wrap').removeClass('parking-gag-content__btns-wrap_active');
        $('#' + pdf).addClass('parking-gag-content__btns-wrap_active')
    })
};

