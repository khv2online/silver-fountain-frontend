window.viewInitializers.about = ($newView) => {
    let $sliderBlock = $("#aboutPageCubeSlider");

    let fitSliderHeight = () => {
        if (getWindowSizeBreakpoint() === "md") {
            let sliderWidth = $sliderBlock.width();
            let sliderDefaultHeight = (sliderWidth / 100) * 83;
            let leftColHeight = $(".text_about_slider:first").parent().height();
            let optimalHeight;
            if (leftColHeight > sliderDefaultHeight) {
                optimalHeight = leftColHeight;
            } else {
                optimalHeight = sliderDefaultHeight;
            }
            $sliderBlock.height(optimalHeight);
        } else {
            $sliderBlock.removeAttr('style');
        }

    };

    let cubeSliderInstance;

    window.requestAnimationFrame(() => {
        fitSliderHeight();
        cubeSliderInstance = new Swiper($sliderBlock, {
            nextButton: $sliderBlock.find(".next:first"),
            prevButton: $sliderBlock.find(".prev:first"),
            buttonDisabledClass: "disable",
        });
    });




    $(window).on("resize.aboutpage", _.debounce(fitSliderHeight, 100));


    $newView.one('replace', () => {
        $(window).off(".aboutpage");
        cubeSliderInstance.destroy();
        cubeSliderInstance = null;

    });
};