window.viewInitializers.hypothec = ($newView) => {

    let breakpoint = getWindowSizeBreakpoint();
    const swiperOptions = {
        speed: 400,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
    };

    let swiperInstance;

    if (breakpoint === 'md' || breakpoint === 'lg') {
        swiperInstance = new Swiper($newView.find('.tradein_page:first .swiper-container'), swiperOptions);
    }

    let tradeinInteractiveForm = new InteractiveForm({
        el: $newView.find('#tradeinForm'),
    });

    let hypothecInteractiveForm = new InteractiveForm({
        el: $newView.find('#hypothecForm'),
    });

    $newView.find("#a-tabs").tabs({
        active: 0,
        hide: {
            effect: "fade",
            duration: 300
        },
        show: {
            effect: 'fade',
            duration: 300
        }
    });

    {
        let $bg = $newView.find('.tradein-bg:first');

        $newView.find('#ui-id-1').on('click', () => {
            $bg.removeClass('tradein_bd_bg--active');
        });

        $newView.find('#ui-id-2').on('click', () => {
            $bg.addClass('tradein_bd_bg--active');
        });
    }

    let switchFooterColor = (breakpoint) => {
        let footer = document.getElementById('ft');
        if (breakpoint === 'sm') {
            footer.classList.remove('b-footer--inverted');
        } else {
            footer.classList.add('b-footer--inverted');
        }
    };

    switchFooterColor(breakpoint);

    $(window).on('resize.hypothecPage orientationchange.hypothecPage', _.debounce(() => {
        // TODO: To get done swiper autoreinit on window resize

        let breakpoint = getWindowSizeBreakpoint();

        switchFooterColor(breakpoint);

        if ((breakpoint === 'md' || breakpoint === 'lg')) {

            swiperInstance = new Swiper('.tradein_page .swiper-container', swiperOptions);
        } else {
            try {
                swiperInstance.destroy(true, true);
            } catch (err) {}

        }

    }, 100)).on('click.hypothecPage', (event) => {
        event.stopPropagation();
        let $el = $(event.target).closest('li');
        if ($el.hasClass('panel--active')) {
            $el.removeClass('panel--active');
        } else {
            $('#hypothec_accordion .panel--active').removeClass('panel--active');
            $el.addClass('panel--active');
        }
    });

    $newView.one('replace', () => {
        $(window).off('.hypothecPage');
        tradeinInteractiveForm.destroy();
        hypothecInteractiveForm.destroy();
        try {
            swiperInstance.destroy(true, true);
        } catch (err) {}
    });
};