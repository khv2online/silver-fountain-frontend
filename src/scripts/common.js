/*=require ./includes/chunks/HOSTCMS-captcha-update.js */
/*=require ./includes/chunks/fancybox_defaults.js */
/*=require ./includes/chunks/tel_href_update.js */
/*=require ./includes/classes/*.js */

window.pagePreloader = new Preloader("#pagePreloader");
window.pagePreloader.show();

// localStorage polyfill for IOS private mode
(function() {
    try {
        localStorage.setItem("_storage_test", "test");
        localStorage.removeItem("_storage_test");
    } catch (exc) {
        var tmp_storage = {};
        var p = "__etalonsilverWebsiteStorage__";
        Storage.prototype.setItem = function(k, v) {
            tmp_storage[p + k] = v;
        };
        Storage.prototype.getItem = function(k) {
            return tmp_storage[p + k] === undefined ? null : tmp_storage[p + k];
        };
        Storage.prototype.removeItem = function(k) {
            delete tmp_storage[p + k];
        };
        Storage.prototype.clear = function() {
            tmp_storage = {};
        };
    }
})();

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (this.name.indexOf("[]") !== -1) {
            if (o[this.name]) {
                o[this.name].push(this.value || "");
            } else {
                o[this.name] = [this.value];
            }
        } else {
            o[this.name] = this.value || "";
        }
    });
    return o;
};

$.widget.bridge("uitooltip", $.ui.tooltip);

function preloadPictures(pictureUrls, everyPictureLoadCallback, callback) {
    var i,
        j,
        loaded = 0;

    for (i = 0, j = pictureUrls.length; i < j; i++) {
        let img = new Image();
        img.onload = () => {
            everyPictureLoadCallback();
            if (++loaded == pictureUrls.length && callback) {
                callback();
            }
        };

        //Use the following callback methods to debug
        //in case of an unexpected behavior.
        //img.onerror = function() {};
        //img.onabort = function() {};

        img.src = pictureUrls[i];
    }
}

function getWindowSizeBreakpoint() {
    let w = window.innerWidth;
    let result = "lg";
    if (w < 768) {
        result = "xs";
    } else if (w > 767 && w < 1024) {
        result = "sm";
    } else if (w > 1023 && w < 1600) {
        result = "md";
    }
    return result;
}

window.viewInitializers = {};
/*=require ./includes/pages/*.js */

function applyNewView($cont, $newView, $oldView) {
    $oldView.trigger("replace");
    $cont.empty().append($newView);
    let oldPageOpts = $oldView.data("page-vars");
    let newPageOpts = $newView.data("page-vars");
    let pageType = newPageOpts.pageType;

    window.requestAnimationFrame(() => {
        document.title = newPageOpts.title;
        if (pageType !== "filters" && pageType !== "flat") {
            let storageKeys = Object.keys(localStorage);
            _.forEach(storageKeys, (key) => {
                if (_.startsWith(key, "savedFlatFilterStates")) {
                    localStorage.removeItem(key);
                }
            });
        }

        let body = document.body;
        body.classList.remove(`l-body--${oldPageOpts.pageType}`);
        body.classList.add(`l-body--${pageType}`);

        let $footer = $("#ft");
        let $header = $("#hd");

        if (newPageOpts.hideControls) {
            $footer.addClass("b-footer--hidden");
            $header.addClass("b-header--hidden");
        } else {
            $footer.removeClass("b-footer--hidden");
            $header.removeClass("b-header--hidden");
        }

        if (newPageOpts.isHeaderWhite) {
            $header.removeClass("b-header--inverted");
        } else {
            $header.addClass("b-header--inverted");
        }

        if (newPageOpts.isFooterWhite) {
            $footer.removeClass("b-footer--inverted");
        } else {
            $footer.addClass("b-footer--inverted");
        }
    });

    $("html,body").scrollTop(0);

    let initFunc = window.viewInitializers[pageType];

    if (initFunc) {
        initFunc($newView);
    }
}
/*
function switchScreen(opts) {
    window.pagePreloader.show();
    let fadeDuration = 0.5;

    $.ajax({
        url: opts.path,
        dataType: "html",
        data: {
            ajaxpage: 1,
        },
    }).always((data) => {
        let $cont = $("#mainView");
        let $oldView = $cont.find(".b-page-content:first");
        let $newView;
        if (data.responseText) {
            $newView = $(data.responseText).find(".b-page-content:first");
        } else {
            $newView = $(data).find(".b-page-content:first");
        }

        $newView.waitForImages({
            waitForAll: true,

            finished: function() {
                if (window.mainMenu.isOpen) {
                    applyNewView($cont, $newView, $oldView);
                    window.mainMenu.hide();
                    window.pagePreloader.hide(() => {
                        $.fancybox.close();
                    });
                } else {
                    window.mainMenu.hide(() => {
                        TweenLite.to($cont, fadeDuration, {
                            opacity: "0",
                            onComplete: () => {
                                applyNewView($cont, $newView, $oldView);
                                TweenLite.to($cont, fadeDuration, {
                                    opacity: "1",
                                    onComplete: () => {
                                        window.pagePreloader.hide(() => {
                                            $.fancybox.close();
                                        });
                                    },
                                });
                            },
                        });
                    });
                }
            },
        });
    });
}
*/
function hoverHeaderMenu() {
    $(".b-header-menu__sublink").hover(
        function() {
            $(".b-header-menu__sublink").addClass("b-header-menu__sublink--color");
        },
        function() {
            $(".b-header-menu__sublink").removeClass("b-header-menu__sublink--color");
        }
    );

    $(".b-header-menu__link").hover(
        function() {
            $(".b-header-menu__link").addClass("b-header-menu__link--color");
        },
        function() {
            $(".b-header-menu__link").removeClass("b-header-menu__link--color");
        }
    );

    $(".b-header-menu__submenu").hover(
        function() {
            $(".b-header-menu__link").addClass("b-header-menu__link--color");
            $(this)
                .parent()
                .find(".b-header-menu__link")
                .addClass("b-header-menu__link--active");
        },
        function() {
            $(".b-header-menu__link").removeClass("b-header-menu__link--color");
            $(this)
                .parent()
                .find(".b-header-menu__link")
                .removeClass("b-header-menu__link--active");
        }
    );
}

$(document).ready(function() {
    window.mainMenu = new MainMenu({
        modalWindowSelector: "#mainMenuModal",
        togglerSelector: ".b-burger:first",
    });

    /*window.app = Sammy();

    app.run();
    app.get("(.*)", function() {
        switchScreen({
            path: this.params.splat,
        });
        try {
            ga("set", "page", this.path);
            ga("send", "pageview");
            yaCounter45266157.hit("http://" + window.location.hostname + this.path);
            _tmr.pageView({ id: "2925613", url: "http://" + window.location.hostname + this.path });
        } catch (err) {
            console.error("Error!!! Can't send statistics\n", err);
        }
    });*/

    (() => {
        let $cont = $(".b-page-content:first");
        let initialPageType = $cont.data("page-vars").pageType;
        let startFunction = window.viewInitializers[initialPageType];
        if (startFunction) {
            startFunction($cont);
        }
    })();

    new InteractiveForm({
        el: "#globalCallmeForm",
    });

    window.pagePreloader.hide();
    hoverHeaderMenu();

    // Обработка баннеров
    (function() {
        let $banner = $('[data-banner]');
        let interval;
        let time = 1;

        countDown();

        function countDown() {
            let i = 0;

            interval = setInterval(function() {
                i = ++i;

                if (i === time) bannersInit();
            }, 1000);

            $(document).on('mousemove', function(e) {
                if (e.clientY == 0) bannersInit();
            });
        }

        function bannersInit() {
            clearInterval(interval);

            $(document).off('mousemove');

            let disableBanner = () => {
                let now            = new Date();
                let midnight       = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 23, 59, 59);
                let daysToMidnight = (midnight - now) / 1000 / 60 / 60 / 24;

                Cookies.set("banner-closed", "1", { expires: daysToMidnight });
            };

            if (!$banner.length) return;

            if (Cookies.get("banner-closed")) return;

            $.fancybox.open($banner, {
                hideScrollbar: false,
                beforeClose: disableBanner,
                baseClass: '_simple',
                hideScrollbar: true,
                afterLoad: function() {
                    $banner.find('.popup-banner__btn').click(function(e) {
                        e.preventDefault();

                        disableBanner();

                        window.location.href = $(this).attr('href');
                    });
                }
            });
        }
    })();

    function amount2Word(amount, words) {
        var resOfHundred = amount % 100;
        var restOfTen = amount % 10;
        var resultString;

        switch (true) {
            case (resOfHundred >= 5 && resOfHundred <= 20):
                resultString = words[2];

                break;
            default:
                switch (true) {
                    case (restOfTen == 1):
                        resultString = words[0];

                        break;
                    case (restOfTen >= 2 && restOfTen <= 4):
                        resultString = words[1];

                        break;
                    default:
                        resultString = words[2];

                        break;
                }
                break;
        }
        return (resultString);
    }

    $('[data-countdown]').each(function() {
        let $countdown = $(this);
        let finalDate  = $countdown.data('countdown');

        function countUpdate(event) {
            let days    = event.strftime('%D');
            let hours   = event.strftime('%H');
            let minutes = event.strftime('%M');

            let $days    = $countdown.find('[data-count-days]');
            let $hours   = $countdown.find('[data-count-hours]');
            let $minutes = $countdown.find('[data-count-minutes]');

            $days
                .html(days)
                .data('count-days', amount2Word(days, ['день', 'дня', 'дней']))
                .attr('data-count-days', amount2Word(days, ['день', 'дня', 'дней']));
            $hours
                .html(hours)
                .data('count-hours', amount2Word(hours, ['час', 'часа', 'часов']))
                .attr('data-count-hours', amount2Word(hours, ['час', 'часа', 'часов']));

            $minutes
                .html(minutes)
                .data('count-minutes', amount2Word(minutes, ['минута', 'минуты', 'минут']))
                .attr('data-count-minutes', amount2Word(minutes, ['минута', 'минуты', 'минут']));
        }

        function countFinish() {
            console.log('finish');
        }

        $countdown.countdown(finalDate)
            .on('update.countdown', countUpdate)
            .on('finish.countdown', countFinish);
    });
});
