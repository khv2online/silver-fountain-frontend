'use strict';
const gulp = require('gulp');
const postcss = require('gulp-postcss');
const del = require('del');
const nunjucksRender = require('gulp-nunjucks-render');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const cssnano = require('gulp-cssnano');
const babel = require('gulp-babel');
const include = require("gulp-include");
const notify = require("gulp-notify");
const cssnext = require("postcss-cssnext");
const gulpif = require('gulp-if');
const sourcemaps = require('gulp-sourcemaps');
const minifyJS = require('gulp-uglify');

//const debug = require('gulp-debug');

const isProduction = true;

gulp.task('templates:all', function() {
    return gulp.src(['src/templates/**/*.html', '!src/templates/layout.html', '!src/templates/includes/**/*.*'])
        //.pipe(debug())
        .pipe(nunjucksRender({
            path: 'src/templates'
        }))
        .on('error', function(err) {
            notify({ title: 'templates task error!' }).write(err.message);
            this.emit('end');
        })
        .pipe(gulp.dest("build"));
});

gulp.task('templates:single', function() {
    return gulp.src(['src/templates/*.html', '!src/templates/layout.html'], { since: gulp.lastRun('templates:single') })
        .pipe(nunjucksRender({
            path: 'src/templates'
        }))
        .on('error', function(err) {
            notify({ title: 'template task error!' }).write(err.message);
            this.emit('end');
        })
        .pipe(gulp.dest("build"));
});

gulp.task('styles', function() {
    var processors = [
        cssnext({
            browsers: "> 0.3%",
        }),
    ];


    return gulp.src('src/styles/*.css')
        //.pipe(debug())

        .pipe(include())
        .pipe(postcss(processors))
        .on('error', function(err) {
            notify({ title: 'CSS task error!' }).write(err.message);
            this.emit('end');
        })
        .pipe(gulpif(isProduction, cssnano({ discardUnused: { fontFace: false }, autoprefixer: false, zindex: false })))


    .pipe(gulp.dest("build/css"));
});

gulp.task('scripts:single', function() {
    return gulp.src('src/scripts/*.js', { since: gulp.lastRun('scripts:single') })
        .pipe(include()).on('error', console.error)
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .on('error', function(err) {
            notify({ title: 'scripts task error!' }).write(err.message);
            this.emit('end');
        })
        .pipe(minifyJS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("build/js"));
});

gulp.task('scripts:all', function() {
    return gulp.src('src/scripts/*.js')
        .pipe(include()).on('error', console.error)
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .on('error', function(err) {
            notify({ title: 'scripts task error!' }).write(err.message);
            this.emit('end');
        })
        .pipe(minifyJS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("build/js"));
});

gulp.task('assets', function() {
    return gulp.src(['src/assets/**'], { since: gulp.lastRun('assets') })
        .pipe(gulp.dest("build"));
});

gulp.task('vendors:js', function() {
    return gulp.src('src/vendor/js/*.js')
        .pipe(concat('vendor.js'))
        .pipe(minifyJS({
            mangle: false,
        }))
        .pipe(gulp.dest("build/js"));
});

gulp.task('vendors:css', function() {
    return gulp.src('src/vendor/css/*.css')
        .pipe(concat('vendor.css'))
        .pipe(gulpif(isProduction, cssnano({
            zindex: false,
            autoprefixer: false,
            discardUnused: { fontFace: false },
            discardComments: {removeAll: true}
        })))
        .pipe(gulp.dest("build/css"));
});

gulp.task('clean', function() {
    return del('build');
});

gulp.task('build', gulp.series('clean', gulp.parallel(
    'templates:all',
    'styles',
    'vendors:js',
    'vendors:css',
    'assets',
    'scripts:all'

)));

gulp.task('reload', function(done) {
    browserSync.reload();
    done();
});

gulp.task('watch', function() {
    gulp.watch([
        'src/templates/layout.html',
        'src/templates/includes/**/*.html'
    ], gulp.series('templates:all', 'reload'));
    gulp.watch(['src/templates/*.html', '!src/templates/layout.html'], gulp.series('templates:single', 'reload'));

    gulp.watch('src/styles/**/*.*', gulp.series('styles'));

    gulp.watch('src/scripts/*.*', gulp.series('scripts:single', 'reload'));
    gulp.watch(['src/scripts/includes/**/*.*'], gulp.series('scripts:all', 'reload'));
    gulp.watch('src/assets/**/*.*', gulp.series('assets'));
    gulp.watch('src/vendor/css/*.css', gulp.series('vendors:css'));
    gulp.watch('src/vendor/js/*.js', gulp.series('vendors:js', 'reload'));
});

gulp.task('serve', function() {
    browserSync.init({
        server: 'build',
        open: false,
        ghostMode: false,
        reloadDebounce: 120,
    });

    browserSync.watch(['build/**/*.*', '!build/*.html', '!build/js/**/*']).on('change', browserSync.reload);
});

gulp.task('validate', function() {
    return gulp.src('build/*.html')
        .pipe(require('gulp-html-validator')({ format: 'html' }))
        .pipe(gulp.dest('./validator-out'));
});


gulp.task('dev', gulp.series('build', gulp.parallel('watch', 'serve')));
